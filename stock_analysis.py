#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May  9 22:57:41 2022

@author: Landry Huet
"""

import pandas as pd
import numpy as np
import pathlib
from matplotlib import pyplot as plt
import plotly.express as px
from sklearn.linear_model import LinearRegression

#pd.options.plotting.backend = "plotly"

oneYear = pd.to_timedelta(8766, 'h')

def load_boursorama(csv_file, name=None, normalize=True):
  """Load a price file from Boursorama
  
  Arguments
  =========
  csv_file: name of the file to be loaded.
  name: name to be given to the series. It will appear as a label on plots
        later on. It is also mandatory in order to do comparisons.
  normalize: whether to normalize the data to the latest price.
  
  Return
  ======
  s: time series with the closing prices of the stock.
  """
  raw_data = pd.read_csv(csv_file, delimiter='\t')
  raw_data['time'] = pd.to_datetime(raw_data.date, format='%d/%m/%Y %H:%M', errors='raise')
  raw_data.set_index('time', drop=True, inplace=True)
  raw_data.sort_index(inplace=True)
  if normalize:
    data = raw_data.clot / raw_data.clot.iloc[-1]
  else:
    data = raw_data.clot
  
  if name is not None:
    data.rename(name, inplace=True)
  return data # Series


def single_analysis(s, start_date=None, end_date=None):
  """Run a simple long-term analysis of a stock series and plot the results.
  
  Arguments
  =========
  s: price series provided with a load function.
  start_date: date at which analysis should be started, as a datetime.
  end_date: date at which the analysis stops.
  
  Return
  ======
  None
  """
  if end_date is None: end_date = s.index.max()
  if start_date is None: start_date = s.index.min()
  filtered = s[(s.index >= start_date) & (s.index <= end_date)]
  ax = filtered.plot(label=s.name)
  
  # Exponential regression
  time = (filtered.index - filtered.index.max()) / oneYear
  logValue = np.log(filtered)
  lm = LinearRegression()
  lm.fit(time.values.reshape(-1,1), logValue)
  yr_rate = np.exp(lm.coef_[0])-1
  reg_y = pd.Series(data=np.exp(lm.predict(time.values.reshape(-1,1))), index=filtered.index)
  reg_y.plot(ax=ax, style='--', label=f'Year rate {yr_rate*100:2.1f} %')
  print(f'Year rate {yr_rate*100:2.1f} %')
  
  # pseudo-regression with start and end values
  lm2 = LinearRegression()
  ratio = filtered[filtered.index.max()] / filtered[filtered.index.min()]
  k = np.log(ratio) / (time.max() - time.min())
  lm2.coef_ = np.array([k])
  lm2.intercept_ = np.log(filtered[filtered.index.max()])
  reg_y2 = pd.Series(data=np.exp(lm2.predict(time.values.reshape(-1,1))), index=filtered.index)
  yr_rate2 = np.exp(lm2.coef_[0])-1
  reg_y2.plot(ax=ax, style='--', label=f'Year rate {yr_rate2*100:2.1f} %')
  
  plt.legend()
  
  return None


def compare_analysis(s1, s2, start_date=None, end_date=None):
  """Compare the time series of two stock prices.
  
  Arguments
  =========
  s1: first stock prices as time series.
  s2: second stock prices as time series.
  start_date: date (as datetime) at which the analysis should start. Defaults
              the earliest common date between the series.
  end_date: date (as datetime) at which the analysis should end. Defaults
              the latest common date between the series.
  """
  if end_date is None:
    end_date = min(s1.index.max(), s2.index.max())
  if start_date is None:
    start_date = max(s1.index.min(), s2.index.min())
  
  s1 = s1[start_date:end_date]
  s2 = s2[start_date:end_date]
  
  n1 = s1.name
  n2 = s2.name
  if n1 == n2:
    raise ValueError('Both series have the same name.')
  
  data = pd.concat([s1,s2], axis=1)
  
  # Normalize to end value
  data = data/data.iloc[-1]
  data.plot()
  
  delta = data.diff().dropna()
  ax = delta.plot.scatter(n1,n2, s=0.5, style='.', label='variations')
  mini = min(delta[n1].min(), delta[n2].min())
  maxi = max(delta[n1].max(), delta[n2].max())
  ax.plot([mini, maxi], [mini, maxi], 'r--', label='first bisect')
  
  lm = LinearRegression()
  lm.fit(delta[n1].values.reshape(-1,1), delta[n2])
  x_fit = np.array([mini,maxi])
  y_fit = lm.predict(x_fit.reshape(-1,1))
  ax.plot(x_fit, y_fit, 'k:')
  
  r = data[n1].corr(data[n2])
  
  ax.set_title(f'daily variations r = {r*100:2.1f} %')
  
  return data

